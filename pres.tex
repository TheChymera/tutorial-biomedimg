\documentclass[xcolor=table,aspectratio=43,dvipsnames]{beamer}
% \documentclass[xcolor=table]{beamer}
\usepackage{bm}
\usepackage[utf8]{inputenc}
% \usepackage[T1]{fontenc}
% \usepackage{cmbright}
% \usepackage[german, english]{babel} % required for rendering German special characters
\usepackage{amsmath}
\usepackage{empheq}
\usepackage{booktabs}
\usepackage{color}
\usepackage{multicol}
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage{multirow} %required for auto-generated tables http://www.tablesgenerator.com/
\usepackage[thinlines]{easytable}
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{grffile} %determine file extension via LAST dot
\usepackage{soul} %strikethrough via \st{}
\usepackage[autoprint=false, gobble=auto, keeptemps=all, pyfuture=all]{pythontex} % create figures on-line directly from python!

\input{pythontex/functions.tex}
\begin{pythontexcustomcode}[begin]{py}
DOC_STYLE = 'pres/main.conf' 
#pytex.add_dependencies(DOC_STYLE, 'pres/img.conf')
\end{pythontexcustomcode}
 
\newcommand{\myhfill}{\hskip0pt plus 1filll}

\DeclareUnicodeCharacter{00A0}{ }
\setbeamersize{text margin left=0.8em,text margin right=0.8em}

\DeclareSIUnit\pixel{px}

\usecolortheme[RGB={199,199,199}]{structure}
\usetheme{Dresden}

\newenvironment{figurehere}
{\def\@captype{figure}}
{}
\makeatother

\definecolor{dy}{RGB}{202,202,0}
\definecolor{mg}{gray}{0.30}
\definecolor{lg}{gray}{0.60}
\definecolor{vlg}{gray}{0.75}

\definecolor{mypurple}{HTML}{8600e9}
\definecolor{mymagenta}{HTML}{d600d6}
\definecolor{mycyan}{HTML}{00a9a9}
\definecolor{mygreen}{HTML}{00df1f}
\definecolor{myblue}{HTML}{0004f7}
\definecolor{myyellow}{HTML}{dab200}
\definecolor{myred}{HTML}{e41f00}

\setbeamercolor{caption name}{fg=lg}
\setbeamercolor{caption}{fg=lg}
\setbeamercolor{author}{fg=lg}
\setbeamercolor{institute}{fg=lg}
\setbeamercolor{date}{fg=lg}
\setbeamercolor{title}{fg=mg}
\setbeamertemplate{caption}{\centering\insertcaption\par}
\setbeamertemplate{navigation symbols}{}

\title[Biomedical Imaging --- Nuclear Imaging]{Biomedical Imaging}
\subtitle{Nuclear Imaging}
\author{Horea-Ioan Ioanas}
\institute{Institute for Biomedical Engineering, ETH and University of Zürich}
\begin{document}
	\begin{frame}
		\titlepage
		\centering\scriptsize [ \href{https://bitbucket.org/TheChymera/tutorial-biomedimg}{\texttt{bitbucket.org/TheChymera/tutorial-biomedimg}} ]
	\end{frame}
	\section{Gamma Camera}
		\subsection{Collimator Point Spread Fun (PSF) and Full Width at Half Maximum (FWHM)}
			\begin{frame}{Collimator Nomenclature}
				\centering
				\begin{minipage}{.44\textwidth}
					\begin{itemize}
						\item x = Well Number
						\item \textcolor{mypurple}{d = Distance between Septa}
						\item \textcolor{mymagenta}{$\bm{\varepsilon}$ = Offset of Source}
						\item \textcolor{mycyan}{p = Perpendicular Distance}
						\item \textcolor{mygreen}{i = Inplane Distance}
						\item \textcolor{myblue}{s = Shadow}
						\item \textcolor{myyellow}{V = Value}
						\item \textcolor{myred}{L = Length of Collimator}
					\end{itemize}
				\end{minipage}
				\begin{minipage}{.55\textwidth}
					\begin{figure}
					\centering
						\includegraphics[width=\textwidth]{img/collimator}
					\end{figure}
				\end{minipage}
			\end{frame}
			\begin{frame}{PSF Equations}
				\vspace{-1em}
				\begin{align}
					V(s) &= \frac{(d-s)}{d} \\
					s(i) &= \frac{L\cdot i}{p} \\
					i(x) &= d \left( \left\lvert x-\frac{\bm{\varepsilon}}{d}\right\rvert -\frac{1}{2} \right)
				\end{align}
				\begin{empheq}[box={\fboxsep=10pt\fbox}]{align}
					\color{red}
					V(x) = 1 - \frac{L\left( \left\lvert x-\frac{\bm{\varepsilon}}{d}\right\rvert -\frac{1}{2} \right)}{p}
				\end{empheq}
				\begin{align}
					V(x) = 1 - \frac{L\left( \frac{\left\lvert\left( \left\lvert x-\frac{\bm{\varepsilon}}{d}\right\rvert -\frac{1}{2}\right) \right\rvert + \left\lvert x-\frac{\bm{\varepsilon}}{d}\right\rvert -\frac{1}{2}}{2} \right)}{p}
				\end{align}
			\end{frame}
			\begin{frame}{PSF Plot \myhfill \large [\texttt{scripts/a1.py}]}
				\py{pytex_fig('scripts/a1.py', label='a1',)}
			\end{frame}
			\begin{frame}{PSF for \SI{-1.5}{\milli\metre} Offset \myhfill \large [\texttt{scripts/a1extra.py}]}
				\py{pytex_fig('scripts/a1extra.py', label='a1_offset',)}
			\end{frame}
			\begin{frame}{FWHM Equations}
				FWHM = the number of wells receiving at least half the maximum signal.
				\begin{align}
					\frac{max(V(x))}{2} &= \frac{1}{2} \\
					\left\lvert x-\frac{\bm{\varepsilon}}{d}\right\rvert &= \frac{p}{2L}+\frac{1}{2} \\
					FWHM &= 1 + \lfloor x_2\rfloor - \lceil x_1\rceil
				\end{align}
				\begin{empheq}[box={\fboxsep=10pt\fbox}]{align}
					\color{red}
					FWHM(p) = 1 + \left\lfloor \frac{p}{2L} +\frac{1}{2} +\frac{\bm{\varepsilon}}{d} \right\rfloor - \left\lceil -\frac{p}{2L} -\frac{1}{2} +\frac{\bm{\varepsilon}}{d} \right\rceil
				\end{empheq}
			\end{frame}
			\begin{frame}{FWHM Plot \myhfill \large [\texttt{scripts/a2.py}]}
				\py{pytex_fig('scripts/a2.py', label='a2')}
			\end{frame}
			\begin{frame}{FWHM for \SI{-1.5}{\milli\metre} Offset \myhfill \large [\texttt{scripts/a2extra.py}]}
				\py{pytex_fig('scripts/a2extra.py', label='a2_offset')}
			\end{frame}
	\section{PET Reconstruction}
		\subsection{Visualizing Positron Emission Tomography (PET) Data via Filtered Back Projection(FBP)}
			\begin{frame}[fragile]{Sinogram Redundance \myhfill \large [\texttt{scripts/b0.py}]}
				\py{pytex_fig('scripts/b0.py', conf='pres/img.conf', label='b0')}
			\end{frame}
			\begin{frame}[fragile]{Image Reconstruction \myhfill \large [\texttt{scripts/b1.py}]}
				\py{pytex_fig('scripts/b1.py', conf='pres/img.conf', label='b1')}
			\end{frame}
			\begin{frame}[fragile]{Sinogram Manipulation \myhfill \large [\texttt{scripts/b2.py}]}
				\py{pytex_fig('scripts/b2.py', conf='pres/img.conf', label='b2')}
			\end{frame}
			\begin{frame}[fragile]{Sinogram Manipulation \myhfill \large [\texttt{scripts/b3.py}]}
				\py{pytex_fig('scripts/b3.py', conf='pres/img.conf', label='b3')}
			\end{frame}
			\begin{frame}[fragile]{Sinogram Manipulation \myhfill \large [\texttt{scripts/b3extra.py}]}
				\py{pytex_fig('scripts/b3extra.py', conf='pres/img.conf', label='b3extra')}
			\end{frame}
			\begin{frame}[fragile]{Sinogram Manipulation \myhfill \large [\texttt{scripts/b4.py}]}
				\py{pytex_fig('scripts/b4.py', conf='pres/img.conf', label='b4')}
			\end{frame}
			\begin{frame}[fragile]{Reducing the Projection Number \myhfill \large [\texttt{scripts/b5.py}]}
				\centering Projections = Theta Samples = Detectors	
				\py{pytex_fig('scripts/b5.py', conf='pres/img_many.conf', label='b5')}
			\end{frame}
			\begin{frame}[fragile]{Root Mean Square Error \myhfill \large [\texttt{scripts/b6.py}]}
				\py{pytex_fig('scripts/b6.py', label='b6')}
			\end{frame}
			\begin{frame}[fragile]{Filters and Frequency Responses\myhfill \large [\texttt{scripts/b7.py}]}
				\py{pytex_fig('scripts/b7.py', conf='pres/img_many.conf', label='b7')}
			\end{frame}
	\section{PET Attenuation Correction}
		\subsection{Enhancing PET Reconstruction with Computer Tomography (CT)}
			\begin{frame}{Attenuation Correction Overview}
				\begin{figure}
				\centering
					\includegraphics[width=0.6\textwidth]{img/c_overview.png}
				\end{figure}
			\end{frame}	
			\begin{frame}{Attenuation Conversion}
				\centering Piecewise function equation determined from plot:
				\begin{align}
					f(x)=
					\begin{cases} 
						x\frac{0.093}{1000} + 0.093 & x \leq 0 \\
						x\frac{0.262-0.093}{3071} + 0.093 & x > 0 \\
					\end{cases}
				\end{align}
			\end{frame}
			\begin{frame}[fragile]{CT Attenuation Sinogram \myhfill \large [\texttt{scripts/c1.py}]}
				Overview Steps 1 and 2.
				\py{pytex_fig('scripts/c1.py', conf='pres/img.conf', label='c1')}
			\end{frame}
			\begin{frame}[fragile]{Applying Attenuation Correction to PET\myhfill \large [\texttt{scripts/c2.py}]}
				Overview Steps \textcolor{vlg}{1, 2,} 3, and 4.
				\py{pytex_fig('scripts/c2.py', conf='pres/img.conf', label='c2')}
			\end{frame}
			\begin{frame}[fragile]{Reconstruction of Corrected Sinogram\myhfill \large [\texttt{scripts/c3.py}]}
				Overview Steps \textcolor{vlg}{1, 2, 3, 4, and} 5.
				\py{pytex_fig('scripts/c3.py', conf='pres/img.conf', label='c3')}
			\end{frame}
			\begin{frame}[fragile]{Visualizing Signal Gain\myhfill \large [\texttt{scripts/c3extra.py}]}
				\py{pytex_fig('scripts/c3extra.py', conf='pres/img.conf', label='c3')}
			\end{frame}
\end{document}
