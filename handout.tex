\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[a4paper, margin=2cm]{geometry}
\usepackage{titling}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{siunitx}
\usepackage{graphicx, float}
\usepackage{bm}
\usepackage{hyperref}

\definecolor{lightgray}{gray}{0.9}
\definecolor{mg}{gray}{0.5}
\definecolor{mypurple}{HTML}{8600e9}
\definecolor{mymagenta}{HTML}{d600d6}
\definecolor{mycyan}{HTML}{00a9a9}
\definecolor{mygreen}{HTML}{00df1f}
\definecolor{myblue}{HTML}{0004f7}
\definecolor{myyellow}{HTML}{dab200}
\definecolor{myred}{HTML}{e41f00}

\sisetup{list-final-separator = {, and }}

\lstset{
	showstringspaces=false,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue},
	commentstyle=\color[grey]{0.7},
	stringstyle=\color[RGB]{255,150,75}
}
\hypersetup{
  colorlinks,
  urlcolor=mg,
  linkcolor=mg,
}

\newcommand{\inlinecode}[2]{\colorbox{lightgray}{\lstinline[language=#1]$#2$}}
\posttitle{\par\end{center}}
\setlength{\droptitle}{-7em}

\usepackage{multicol}
\setlength{\columnsep}{1em}

\renewcommand{\thesection}{\Alph{section}}

\newcommand{\myhfill}{\hskip0pt plus 1filll}

\title{Biomedical Imaging --- Nuclear Imaging\vspace{-1em}}
\date{\today}

\begin{document}
\maketitle

The following tasks should be completed with the help of associated computer scripts and example data, which you can find under \href{http://chymera.eu/docs/tutorial-biomedimg_exercises.zip}{\texttt{chymera.eu/docs/tutorial-biomedimg\_exercises.zip}}.
The exercise scripts (with key lines removed) are located under the \textcolor{mg}{\texttt{obfuscated}} directory.
You will receive the \textcolor{mg}{\texttt{scripts}} directory with the fully functional example solutions during the post-discussion.
You have the choice of addressing these tasks with Python, Octave, or MATLAB.
The respective interpreters can be used as follows:

\begin{itemize}
	\footnotesize
	\setlength\itemsep{0.05em}
	\item Python: From the terminal, e.g. \inlinecode{bash}{python -c 'import a1; import matplotlib.pyplot as plt; plt.show()'}.
	\item Octave: From the terminal, e.g. \inlinecode{bash}{octave a1.m}.
	\vspace{0.45em}
	\item MATLAB: Open and run the script in the graphical interface.
\end{itemize}

\begin{multicols}{2}
\section{Gamma Camera}
	Calculate the Point-Spread Function (PSF) of a gamma camera with a square array of collimators (of length $L=$ \SI{20}{\milli\metre} and width $d=$ \SI{4}{\milli\metre}) as a function of the perpendicular distance $p$ of the point source from the collimator surface.
	We assume that the tungsten septa have infinite absorption and are of negligible width.
	For demonstration purposes assume that the camera is comprised of 15 detector elements.

	Please mind the given nomenclature in your work, for ease of comparison:
	\begin{itemize}
		\setlength\itemsep{0.05em}
		\item x = Well Number
		\item \textcolor{mypurple}{d = Distance between Septa}
		\item \textcolor{mymagenta}{$\bm{\varepsilon}$ = Offset of Source}
		\item \textcolor{mycyan}{p = Perpendicular Distance}
		\item \textcolor{mygreen}{i = Inplane Distance}
		\item \textcolor{myblue}{s = Shadow}
		\item \textcolor{myyellow}{V = Value}
		\item \textcolor{myred}{L = Length of Collimator}
	\end{itemize}

	\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{img/collimator}
	\caption{Diagram of the nomenclature.}
	\end{figure}

	\subsection{\myhfill \small [\texttt{scripts/a1}]}
	Compute the normalized PSF (maximum intensity = 1) on the detector axis for perpendicular source distances $p$ in the range \SIrange{1}{20}{\centi\metre} (Step size: \SI{1}{\centi\metre}).
	All 20 normalized intensity curves should be plotted as $V(x)$.

	\subsection{\myhfill \small [\texttt{scripts/a2}]}
	For collimator lengths \SIlist{5; 10; 15; 20}{\milli\metre} plot the full width at half maximum (FWHM) as a function of $p$.
	Use $x$ as unit on the vertical axis.
	How does the offset affect the FWHM?

\section{PET Reconstruction}
	The objective of the exercise is to explore the process of PET data reconstruction (from a sinogram) implementing the inverse Radon transform for backprojection (BP), and filtered backprojection (FBP).
	Different filter settings in FBP, as well as sinogram corruption and the number of detectors and their impact on spatial resolution will be investigated.

	\subsection{\myhfill \small [\texttt{scripts/b1}]}
	Perform an image reconstruction from the sinogram using simple back projection, a filtered back projection, and compare to the original image.

	\subsection{\myhfill \small [\texttt{scripts/b2}]}
	Delete the central row of the sinogram and reconstruct the image.
	What is the effect of deleting the central row?

	\subsection{\myhfill \small [\texttt{scripts/b3}]}
	Delete a non-central row of the sinogram and reconstruct the image.
	What is the effect of deleting this row?

	\subsection{\myhfill \small [\texttt{scripts/b4}]}
	Delete the central column of the sinogram and reconstruct the image.
	What is the effect of deleting the central column?

	\subsection{\myhfill \small [\texttt{scripts/b5}]}\label{pr.5}
	Repeat the same process, this time, \textit{keeping} only each second, third, fourth, etc. column.
	To what is this equivalent, in terms of imaging hardware?

	\subsection{\myhfill \small [\texttt{scripts/b6}]}
	Compute the Root Mean Square Error (RMSE) of the images obtained in the previous step and the original image (\textcolor{mg}{\texttt{data/phantom.png}}).
	Plot the RMSE as as a function number of preserved columns from~\ref{pr.5}.

	\subsection{\myhfill \small [\texttt{scripts/b7}]}
	Using the different filter options for the backprojection function, find the best filter for filtered back projection and plot its frequency response.
	You can use the RMSE as a goodness criterion.

\section{Attenuation Correction}
	The objective of this exercise is to perform a CT-based attenuation correction of a PET dataset.
	The workflow is shown in figure~\ref{fig:overview}.

	\begin{figure}[H]
	\centering
	\includegraphics[width=0.34\textwidth]{img/attenuation}
	\caption{Conversion scale to transform CT Hounsfield units (HU) into attenuation coefficients [\SI{}{\per\cm}] at \SI{511}{\kilo\electronvolt}.}
	\label{fig:attenuation}
	\end{figure}

	\subsection{\myhfill \small [\texttt{scripts/c1}]}
	Convert the CT intensity image (Hounsfield units [HU]) into attenuation coefficients using the relationship in figure~\ref{fig:attenuation}.
	Convert the resulting CT image into a sinogram.
	Plot the results (this corresponds to overview steps 1 and 2).

	\subsection{\myhfill \small [\texttt{scripts/c2}]}
	Using the Beer-Lambert Law, convert the sinogram of the CT attenuation map into attenuation correction factors (this corresponds to overview step 3).
	Adjust the sinogram of the uncorrected PET image with the attenuation correction factors (this corresponds to overview step 4).
	Plot the PET sinogram before and after correction.

	\subsection{\myhfill \small [\texttt{scripts/c3}]}
	Reconstruct the sinogram to obtain the attenuation-corrected PET image (this corresponds to overview step 5).
	Compare this to uncorrected PET reconstruction.
	Is it really better? Why?
\section*{Questions?}
	\begin{itemize}
		\setlength\itemsep{0.05em}
		\item Horea Christian: \href{mailto:ioanas@biomed.ee.ethz.ch}{ioanas@biomed.ee.ethz.ch}
		\item Felix Schlegel: \href{mailto:felix.schlegel@alumni.ethz.ch}{felix.schlegel@alumni.ethz.ch}
		\item Markus Rudin: \href{mailto:rudin@biomed.ee.ethz.ch}{rudin@biomed.ee.ethz.ch}
	\end{itemize}

\end{multicols}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{img/c_overview.png}
	\caption{Attenuation correction overview. The desaturated steps have already been performed, you will be concentrating on the red tinted steps.}
	\label{fig:overview}
\end{figure}

\end{document}
