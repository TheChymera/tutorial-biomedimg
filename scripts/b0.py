import matplotlib.pylab as plt
import numpy as np
from copy import deepcopy
from os import path
from scipy.io import loadmat

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the sinogram.
sinogram = loadmat(path.join(data_dir,"phantom_sinogram.mat"))['arr']

# We need to copy the array so as to not modify the original.
sinogram_ = deepcopy(sinogram)

# We subtract the first 180 degree spectrum from the latter.
half = int(sinogram.shape[1]/2)
sinogram_[:,half:] = sinogram_[:,half:]-sinogram_[::-1,:half]

# To not distort the heatmap we null entries which are below the original minimum.
# This makes the corrected half will look closer to null than it actually is.
sinogram_[sinogram_ < 0] = np.min(sinogram)

# This takes care of the plotting.
# It should "just work" if provided with the correct kind of data.
fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
ax1.imshow(sinogram,extent=(0, 360, 0, sinogram.shape[0]), aspect='auto')
ax2.imshow(sinogram_,extent=(0, 360, 0, sinogram_.shape[0]), aspect='auto')
ax1.set_title("Full 360 Degree Spectrum")
ax1.set_xlabel("Projection Angle [deg]")
ax1.set_ylabel("Projection position [pixels]")
ax2.set_title("Second Half Corrected for First")
ax2.set_xlabel("Projection Angle [deg]")
