% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
pkg load image

% We load the sinogram.
sinogram = load('../data/phantom_sinogram_padded.mat');
sinogram = getfield(sinogram, 'arr');

% We subtract the first 180 degree spectrum from the latter.
sinogram_shape = size(sinogram);
maximum = sinogram_shape(2);
half = round(maximum/2);
sinogram = sinogram(:,1:half);

% We delete a total number of 60 sinogram columns, this is to better visualize the effect.
sinogram(:,150:250)=0;

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., half);

% We perform a reconstruction.
reconstruction = iradon(sinogram, theta, 'linear', 'None');

% This takes care of the plotting.
% It should 'just work' if provided with the correct kind of data.
h = figure();
subplot(1,2,1);
imshow(sinogram,[]);
title('Sinogram');
subplot(1,2,2);
imshow(reconstruction,[]);
title('Backprojection');
waitfor(h);
