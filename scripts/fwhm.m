function value = fwhm(septa_length, perpendicular_distance, diameter_well, relative_offset)
	% We define a function which calculates the number of wells receiving at least half the maximum value, based on collimator parameters.
	
	% We calculate the intercept of y=0.5 with the point spread function.
	intercept = perpendicular_distance/(2.*septa_length)+1/2.;

	% We subtract the lowest well number to receive at least half the maximum value from the highest.
	value = 1+floor(intercept+(relative_offset/diameter_well))-ceil(-intercept+(relative_offset/diameter_well));
end

