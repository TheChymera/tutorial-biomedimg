import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat, savemat
from skimage.transform import iradon, radon

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the CT image.
ct = loadmat(path.join(data_dir,"head_CTimage.mat"))['arr']

# To allow float manipulations we need to set the dtype to float.
# (the CT image is in integer Hounsfield units)
ct = ct.astype(np.float64)

# We set the intercept here, as it is equal for both slopes
intercept = 0.093

for x in np.nditer(ct, op_flags=['readwrite']):
	if x <= 0:
		slope = 0.093/1000
	else:
		slope = (0.262-0.093)/3071
	x[...] = x*slope + intercept

# We define an angular range with a given sampling rate (which can differ from the range).
theta = np.linspace(0., 180., max(ct.shape), endpoint=False)

# We create the sinogram (this emulates the process which would happen during the PET measurement).
sinogram = radon(ct, theta=theta, circle=True)

# This takes care of the plotting.
# It should "just work" if provided with the correct kind of data.
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(ct)
ax2.imshow(sinogram,extent=(0, 180, 0, sinogram.shape[0]), aspect='auto')
ax1.set_title("CT Attenuation Map")
ax1.get_xaxis().set_visible(False)
ax1.get_yaxis().set_visible(False)
ax2.set_title("CT Sinogram")
ax2.set_xlabel("Projection Angle [deg]")
ax2.set_ylabel("Projection position [pixels]")
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position('right')
