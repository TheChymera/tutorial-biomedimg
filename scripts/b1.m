% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
pkg load image

% We load the original image.
image = imread('../data/phantom.png');

% We make the image grayscale.
image = double(rgb2gray(image));

% We resize the image to correspond to the backprojection obtained from the MATLAB/Octave `iradon()`.
image = image(60:341,60:341);

% We load the sinogram.
sinogram = load('../data/phantom_sinogram.mat');
sinogram = getfield(sinogram, 'arr');

% We subtract the first 180 degree spectrum from the latter.
sinogram_shape = size(sinogram);
maximum = sinogram_shape(2);
half = round(maximum/2);
sinogram = sinogram(:,1:half);

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., half);

% We perform a reconstruction, assuming that the sinogram was correctly generated, containng data only from a circularly shaped matrix, and no artificial padding.
% The iradon function from MATLAB/Octave clips the picture (unlike the `skimage.transform.iradon()` for Python).
% Can you figure our why this might be?
reconstruction = iradon(sinogram, theta, 'linear', 'None');
reconstruction_fbp = iradon(sinogram, theta, 'linear', 'Ram-Lak');

% This takes care of the plotting.
% It should 'just work' if provided with the correct kind of data.
h = figure();
subplot(1,3,1);
imshow(reconstruction,[]);
title('Backprojection');
subplot(1,3,2);
imshow(reconstruction_fbp);
title('FBP');
subplot(1,3,3);
imshow(image,[]);
title('Original');
waitfor(h);
