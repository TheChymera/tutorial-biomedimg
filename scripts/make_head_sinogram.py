import numpy as np
from skimage.io import imread
from skimage.transform import radon
from scipy.io import loadmat, savemat
from os import path

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the image.
image = loadmat(path.join(data_dir+"/head_PETimage.mat"))['arr']

# We define an angular range with a given sampling rate (which can differ from the range).
theta = np.linspace(0., 180., max(image.shape), endpoint=False)

# We create the sinogram (this emulates the process which would happen during the PET measurement).
sinogram = radon(image, theta=theta, circle=True)
sinogram_padded = radon(image, theta=theta, circle=False)

# We save the array in `.mat` format (to preserve MATLAB compatibility).
savemat(path.join(data_dir,'head_sinogram.mat'), mdict={'arr': sinogram})
savemat(path.join(data_dir,'head_sinogram_padded.mat'), mdict={'arr': sinogram_padded})
