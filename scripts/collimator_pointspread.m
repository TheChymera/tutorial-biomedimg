function value = collimator_pointspread(well_number,length_septa,diameter_well,perpendicular_distance,relative_offset)
        % We define a point spread function taking the well number as a positional (obligatory) argument,
        % and all other variables of our model as keyword (optional) arguments.

        % We define the inplane distance as a variable of the well number.
        inplane_distance = diameter_well* (abs(well_number-relative_offset/diameter_well)-0.5);

        % The inplane distance cannot be negative.
        % This is because the final value cannot be negative.
        if inplane_distance < 0
                inplane_distance = 0;
        end
                
        % Solved via triangle similarity.
        shadow = length_septa*inplane_distance/perpendicular_distance;

        % We calculate the value based on the shadow.
        value = (diameter_well-shadow)/diameter_well;

        % Even if the shadow would extend further than the diameter well, the value cannot be negative.
        if value < 0
                value = 0;
        end
end
