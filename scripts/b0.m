% We load the sinogram.
sinogram = load('../data/phantom_sinogram.mat');
sinogram = getfield(sinogram, 'arr');

% We need to copy the array so as to not modify the original.
sinogram_ = sinogram;

% We subtract the first 180 degree spectrum from the latter.
sinogram_shape = size(sinogram);
maximum = sinogram_shape(2);
half = round(maximum/2);
sinogram_(:,half+1:maximum) = sinogram_(:,half+1:maximum)-flipud(sinogram_(:,1:half));

% To not distort the heatmap we null entries which are below the original minimum.
% This makes the corrected half will look closer to null than it actually is.
sinogram_(sinogram_ < 0) = min(min(sinogram));

% This takes care of the plotting.
% It should 'just work' if provided with the correct kind of data.
h = figure();
subplot(1,2,1);
imshow(sinogram, []);
xlabel('Projection Angle [deg]');
ylabel('Projection position [pixels]');
title('Full 360 Degree Spectrum');
subplot(1,2,2);
imshow(sinogram_, []);
xlabel('Projection Angle [deg]');
title('Second Half Corrected for First');
waitfor(h);
