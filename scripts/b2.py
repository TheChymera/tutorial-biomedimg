import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat
from skimage.transform import iradon

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the sinogram.
sinogram = loadmat(path.join(data_dir,"phantom_sinogram.mat"))['arr']

# We select only the first 180 degree angular range
half = int(sinogram.shape[1]/2)
sinogram = sinogram[:,:half]

# We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = np.linspace(0., 180., max(sinogram.shape), endpoint=False)

# We delete the central row of the sinogram.
sinogram[200,:]=0

# We perform a reconstruction, assuming that the sinogram was correctly generated, containng data only from a circularly shaped matrix, and no artificial padding.
reconstruction = iradon(sinogram, theta=theta, circle=True, filter=None)
reconstruction_fbp = iradon(sinogram, theta=theta, circle=True)

# This takes care of the plotting.
# It should "just work" if provided with the correct kind of data.
fig, (ax1, ax2, ax3) = plt.subplots(1, 3,sharey=True)
ax1.imshow(sinogram,extent=(0, 180, 0, sinogram.shape[0]), aspect='auto')
ax2.imshow(reconstruction)
ax3.imshow(reconstruction_fbp, vmin=-0.5, vmax=2.4)
ax1.set_title("Sinogram")
ax1.set_xlabel("Projection Angle [deg]")
ax1.set_ylabel("Projection position [pixels]")
ax2.set_title("Backprojection")
ax3.set_title("FBP (Scaled)")
for ax in (ax2,ax3):
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
