% This function uses the `fwhm()` function from the `fwhm.m` file (in this same directory).
% In order to understand/enable this function, you will also need to inspect that file.

% We define a range of light source distances.
x = linspace(10,200,96);

h = figure();
hold on;
% We iterate over different collimator lengths.
for i = [5,10,15,20]
	y = [];
	for j = x
		% using the fwhm function.
		my_y = fwhm(i, j, 4, -1.5);
		y = cat(2,y,my_y);
	end
	plot(x,y);
end
hold off;

xlabel('Distance [mm]');
ylabel('FWHM [wells]');
waitfor(h);
