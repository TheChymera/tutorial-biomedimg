% This function uses the `collimator_pointspread()` function from the `colimator_pointspread.m` file (in this same directory).
% In order to understand/enable this function, you will also need to inspect that file.

% We define a well number range.
x = linspace(-10,10,21);

h = figure();
hold on;
for i = linspace(10,200,20)
        y = [];
        for j = x
                my_y = collimator_pointspread(round(j), 20, 4, i, 0);
                y = cat(2,y,my_y);
        end
        plot(x,y,'o-');
end
hold off;

xlabel('Well [ordinal]');
ylabel('Value [relative]');
waitfor(h);
