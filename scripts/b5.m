% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
pkg load image

% We load the sinogram.
sinogram = load('../data/phantom_sinogram_padded.mat');
sinogram = getfield(sinogram, 'arr');

% We subtract the first 180 degree spectrum from the latter.
sinogram_shape = size(sinogram);
maximum = sinogram_shape(2);
half = round(maximum/2);
sinogram = sinogram(:,1:half);

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., half);

h = figure();
for i = 1:10
	% We reduce the sinogram correspondingly.
	sinogram_ = sinogram(:,1:i:half);
	
	% We perform a reconstruction.
	reconstruction_fbp = iradon(sinogram_, theta(1:i:half), 'linear', 'Ram-Lak');
	
	% This takes care of the plotting.
	% It should 'just work' if provided with the correct kind of data.
	subplot(2,5,i);
	imshow(reconstruction_fbp,[]);
	title(strcat('#Detectors',num2str(i)));
end
waitfor(h);
