% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
%pkg load image

% We load the original image.
image = imread('../data/phantom.png');

% We make the image grayscale.
image = double(rgb2gray(image));

% We normalize the 256-bit value to 1.
image = image / 255;

% We load the sinogram.
sinogram = load('../data/phantom_sinogram_padded.mat');
sinogram = getfield(sinogram, 'arr');

% We subtract the first 180 degree spectrum from the latter.
sinogram_shape = size(sinogram);
maximum = sinogram_shape(2);
half = round(maximum/2);
sinogram = sinogram(:,1:half);

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., half);

filters = {'Ram-Lak', 'Shepp-Logan', 'Cosine', 'Hamming', 'Hann'};

% We iterate over 10 detector reduction steps.
h = figure();
for i =1:5
	% We reconstruct the image with the appropriate filter.
	[reconstruction_fbp, response] = iradon(sinogram, theta, 'linear', filters{i});

	% We calculate the Root Mean Square Error.
	difference = reconstruction_fbp-image;
	rmse = sqrt(mean(mean(difference^2)));

	% This takes care of the plotting.
	% It should 'just work' if provided with the correct kind of data.
	subplot(2,5,i);
	imshow(reconstruction_fbp,[]);
	title(strcat(filters{i},'FBP RMSE=',num2str(sprintf('%.3f',rmse))));

	subplot(2,5,i+5);
	plot(response);
end
waitfor(h);
