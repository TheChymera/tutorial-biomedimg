import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat, savemat
from skimage.transform import radon, iradon

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the CT image.
ct = loadmat(path.join(data_dir,"head_CTimage.mat"))['arr']

# To allow float manipulations we need to set the dtype to float.
# (the CT image is in integer Hounsfield units)
ct = ct.astype(np.float64)

# We load the PET sinogram.
pet_sinogram = loadmat(path.join(data_dir,"head_sinogram.mat"))['arr']

# We set the intercept here, as it is equal for both slopes
intercept = 0.093

for x in np.nditer(ct, op_flags=['readwrite']):
	if x <= 0:
		slope = 0.093/1000
	else:
		slope = (0.262-0.093)/3071
	x[...] = x*slope + intercept

# We define an angular range with a given sampling rate (which can differ from the range).
theta = np.linspace(0., 180., max(ct.shape), endpoint=False)

# We create the sinogram (this emulates the process which would happen during the PET measurement).
ct_sinogram = radon(ct, theta=theta, circle=True)

# We take the exponent of the normalized attenuation factor.
attenuation = np.exp(-(ct_sinogram*0.234))

# We apply the attenuation correction to the PET sinogram.
corrected_sinogram = np.divide(pet_sinogram,attenuation)

# We reconstruct images from both the original and the corrected sinograms.
reconstruction = iradon(pet_sinogram, theta=theta, circle=True)
corrected_reconstruction = iradon(corrected_sinogram, theta=theta, circle=True)

# We calculate the difference between the two images.
difference = corrected_reconstruction - reconstruction

# The above difference is unsatisfactory.
# It is important that they are both demeaned, as the corrected image has higher absolute signal than the original image.
difference_norm = corrected_reconstruction/np.mean(corrected_reconstruction) - reconstruction/np.mean(reconstruction)

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(difference)
ax2.imshow(difference_norm)
ax1.set_title("Corrected FBP - FBP")
ax2.set_title("norm(Corrected FBP) - norm(FBP)")
for ax in (ax1, ax2):
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
