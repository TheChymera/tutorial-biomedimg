import numpy as np
import matplotlib.pyplot as plt

# We define a function which calculates the number of wells receiving at least half the maximum value, based on collimator parameters.
def fwhm(septa_length=20,
	perpendicular_distance=10,
	diameter_well=4,
	relative_offset=0,
	):
	import numpy as np

	# We calculate the intercept of y=0.5 with the point spread function.
	intercept = perpendicular_distance/(2.*septa_length)+1/2.

	# We subtract the lowest well number to receive at least half the maximum value from the highest.
	value = 1+np.floor(intercept+(relative_offset/diameter_well))-np.ceil(-intercept+(relative_offset/diameter_well))
	return value

# We define a range of light source distances.
x = np.linspace(10,200,96)

fig, ax = plt.subplots()
leg = []

# This takes care of the plotting.
# It should "just work" if provided with the correct kind of data.
# We iterate over different collimator lengths.
for i in [5,10,15,20]:
	y = []
	for j in x:
		y.append(fwhm(perpendicular_distance=j, septa_length=i))
	ax.plot(x,y,'-o',markersize=1.5)
	leg.append(int(i))
plt.legend(leg, title='Length\n [mm]')
plt.xlabel('Distance [mm]')
plt.ylabel('FWHM [wells]')
