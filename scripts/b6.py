import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat
from skimage.io import imread
from skimage.transform import iradon
from copy import deepcopy

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the original image.
image = imread(path.join(data_dir+"/phantom.png"), as_grey=True)

# We load the sinogram.
sinogram = loadmat(path.join(data_dir,"phantom_sinogram.mat"))['arr']

# We select only the first 180 degree angular range
half = int(sinogram.shape[1]/2)
sinogram = sinogram[:,:half]

# We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = np.linspace(0., 180., max(sinogram.shape), endpoint=False)

# We define a plot.
fig, ax = plt.subplots()

# We create lists to populate.
x = []
y = []

# We iterate over 10 detector reduction steps.
for j in range(10):

	# We do not start counting with 0.
	i = j+1

	# We reconstruct the image with the appropriate reduction.
	reconstruction_fbp = iradon(sinogram[:,::i], theta=theta[::i], circle=True)

	# We calculate the Root Mean Square Error.
	rmse = np.sqrt(np.mean((reconstruction_fbp-image)**2))

	# We append the x and y value to the respective lists.
	x.append(sinogram[:,::i].shape[1])
	y.append(rmse)

# We plot the corresponding x and y values.
ax.plot(x,y,'-o',markersize=1.5)

# We add axis labels.
plt.xlabel('\#Detectors')
plt.ylabel('RMSE (Ramp FBP)')
