% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
pkg load image

% We load the CT image.
ct = load('../data/head_CTimage.mat');
ct = getfield(ct, 'arr');

% To allow float manipulations we need to set the dtype to float.
% (the CT image is in integer Hounsfield units)
ct = double(ct);

% We set the intercept here, as it is equal for both slopes
%%???%%

for i=1:size(ct,1) %rows
	for k=1:size(ct,2) %columns
		if ct(i,k) <= 0
			%%???%%
		else
			%%???%%
		end
		ct(i,k)=ct(i,k)*slope + intercept;
	end
end

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., size(ct,2));

% We create the sinogram (this emulates the process which would happen during the PET measurement).
%%???%%

% This takes care of the plotting.
% It should 'just work' if provided with the correct kind of data.
h = figure();
subplot(1,2,1);
imshow(ct,[]);
title('CT Attenuation Map');
subplot(1,2,2);
imshow(sinogram,[]);
title('CT Sinogram');
waitfor(h);
