% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
pkg load image

% We load the original image.
image = imread('../data/phantom.png');

% We make the image grayscale.
image = double(rgb2gray(image));

% We normalize the 256-bit value to 1.
image = image / 255;

% We load the sinogram.
sinogram = load('../data/phantom_sinogram_padded.mat');
sinogram = getfield(sinogram, 'arr');

% We subtract the first 180 degree spectrum from the latter.
sinogram_shape = size(sinogram);
maximum = sinogram_shape(2);
half = round(maximum/2);
sinogram = sinogram(:,1:half);

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., half);

% We create lists to populate.
x = [];
y = [];

h = figure();
for i = 1:10
	% We reduce the sinogram correspondingly.
	%%???%%

	% We perform a reconstruction.
	%%???%%

	% We calculate the Root Mean Square Error.
	%%???%%

	% We determine the number of detectors.
	sinogram_size = size(sinogram_);
	detectors = sinogram_size(2);

	% We append the x and y value to the respective lists.
	x = cat(2,x,detectors);
	y = cat(2,y,rmse);	
end
% We plot the corresponding x and y values.
plot(x,y);

% We add axis labels.
xlabel('#Detectors');
ylabel('RMSE (Ramp FBP)');
waitfor(h);
