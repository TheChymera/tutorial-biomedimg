import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

# We define a point spread function taking the well number as a positional (obligatory) argument,
# and all other variables of our model as keyword (optional) arguments.
def collimator_pointspread(well_number,
	length_septa=20,
	diameter_well=4,
	perpendicular_distance=20,
	relative_offset=0.0,
	):
	import numpy as np

	# We define the inplane distance as a variable of the well number.
	##???##

	# The inplane distance cannot be negative.
	# This is because the final value cannot be negative.
	if inplane_distance < 0:
		inplane_distance = 0

	# Solved via triangle similarity.
	##???##

	# We calculate the value based on the shadow.
	value = (diameter_well-shadow)/diameter_well

	# Even if the shadow would extend further than the diameter well, the value cannot be negative.
	##???##

	return value

# We define a well number range.
x = np.linspace(-10,10,21)

# This takes care of the plotting.
# It should "just work" if provided with the correct kind of data.
fig, ax = plt.subplots()
leg = []
for i in np.linspace(10,200,20):
	y = []
	for j in x:
		y.append(collimator_pointspread(int(j), perpendicular_distance=i))
	ax.plot(x,y,'-o')
	leg.append(int(i))
plt.legend(leg, title='Distance\n [mm]')
plt.xlabel('Well [ordinal]')
plt.ylabel('Value [relative]')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
