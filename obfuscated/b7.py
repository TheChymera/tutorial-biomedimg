import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat
from skimage.io import imread
from skimage.transform import iradon
from copy import deepcopy

# We copy the relevant bits from the `skimage.transform.iradon` function, located at:
# https://github.com/scikit-image/scikit-image/blob/5a0c5a5f03438ae483b2fc9612c9d634c74c2786/skimage/transform/radon_transform.py#L222
def frequency_response(filter, radon_image):
	import numpy as np
	from scipy.fftpack import fftfreq

	projection_size_padded = \
        max(64, int(2 ** np.ceil(np.log2(2 * radon_image.shape[0]))))
	f = fftfreq(projection_size_padded).reshape(-1, 1)
	omega = 2 * np.pi * f                                # angular frequency
	fourier_filter = 2 * np.abs(f)                       # ramp filter
	if filter == "ramp":
		pass
	elif filter == "shepp-logan":
		# Start from first element to avoid divide by zero
		fourier_filter[1:] = fourier_filter[1:] * np.sin(omega[1:]) / omega[1:]
	elif filter == "cosine":
		fourier_filter *= np.cos(omega)
	elif filter == "hamming":
		fourier_filter *= (0.54 + 0.46 * np.cos(omega / 2))
	##???##

	return fourier_filter

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the original image.
image = imread(path.join(data_dir+"/phantom.png"), as_grey=True)

# We load the sinogram.
sinogram = loadmat(path.join(data_dir,"phantom_sinogram.mat"))['arr']

# We define an a 0-180 degree angular range sampled corresponding to the sinogram.
theta = np.linspace(0., 360., max(sinogram.shape), endpoint=False)

filters = ['ramp', 'shepp-logan', 'cosine', 'hamming', 'hann']
fig, ax = plt.subplots(2, len(filters), sharey='row')

# We iterate over 10 detector reduction steps.
for ix, f in enumerate(filters):

	# We reconstruct the image with the appropriate filter.
	##???##

	# We calculate the Root Mean Square Error.
	##???##

	# This takes care of the plotting.
	# It should "just work" if provided with the correct kind of data.
	ax[0,ix].imshow(reconstruction_fbp)
	ax[0,ix].set_title('{} FBP\nRMSE={:.1}'.format(f,rmse))
	ax[0,ix].get_xaxis().set_visible(False)
	ax[0,ix].get_yaxis().set_visible(False)

	response = frequency_response(f, sinogram)
	ax[1,ix].plot(response)
