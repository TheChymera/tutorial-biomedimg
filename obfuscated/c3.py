import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat, savemat
from skimage.transform import radon, iradon

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the CT image.
ct = loadmat(path.join(data_dir,"head_CTimage.mat"))['arr']

# We determine the attenuation coefficient scaling factor given our data.
##???##

# To allow float manipulations we need to set the dtype to float.
# (the CT image is in integer Hounsfield units)
ct = ct.astype(np.float64)

# We load the PET sinogram.
pet_sinogram = loadmat(path.join(data_dir,"head_sinogram.mat"))['arr']

# We set the intercept here, as it is equal for both slopes
##???##

for x in np.nditer(ct, op_flags=['readwrite']):
	if x <= 0:
		##???##
	else:
		##???##
	x[...] = x*slope + intercept

# We define an angular range with a given sampling rate (which can differ from the range).
theta = np.linspace(0., 180., max(ct.shape), endpoint=False)

# We create the sinogram (this emulates the process which would happen during the PET measurement).
##???##

# We take the exponent of the normalized attenuation factor.
##???##

# We apply the attenuation correction to the PET sinogram.
corrected_sinogram = np.divide(pet_sinogram,attenuation)

# We reconstruct images from both the original and the corrected sinograms.
##???##
##???##

# This takes care of the plotting.
# It should "just work" if provided with the correct kind of data.
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(reconstruction)
ax2.imshow(corrected_reconstruction)
ax1.set_title("FBP")
ax2.set_title("Corrected FBP")
for ax in (ax1, ax2):
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
