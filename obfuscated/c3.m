% This script requires the iradon function - which is included in MATLAB but only available for Octave via the Octave Forge image package.
% If you are trying to execute this script with Octave, make sure the image package is installed `pkg install -forge image`, and make sure the following line is uncommented (if using MATLAB, make sure it is commented).
pkg load image


% We load the PET image in order to create a sinogram.
% This is because MATLAB pads the data with slightly more pixels than Python, so we cannot use the provided PET sinogram.
pet_data = load('../data/head_PETimage.mat');
pet = getfield(pet_data, 'arr');

% We load the CT image.
ct_data = load('../data/head_CTimage.mat');
ct = getfield(ct_data, 'arr');

% We determine the attenuation coefficient scaling factor given our data.
%%???%%

% To allow float manipulations we need to set the dtype to float.
% (the CT image is in integer Hounsfield units)
ct = double(ct);

% We set the intercept here, as it is equal for both slopes
%%???%%

for i=1:size(ct,1) %rows
	for k=1:size(ct,2) %columns
		if ct(i,k) <= 0
			%%???%%
		else
			%%???%%		
		end
		ct(i,k)=ct(i,k)*slope + intercept;
	end
end

% We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = linspace(0., 180., size(ct,2));

% We create the sinogram (this emulates the process which would happen during the PET measurement).
%%???%%

% PET forward projection.
% This emulates the process which would happen during the actual PET measurement.
%%???%%

% We take the exponent of the normalized attenuation factor.
%%???%%

% We apply the attenuation correction to the PET sinogram.
corrected_sinogram = pet_sinogram./attenuation;

% We reconstruct images from both the original and the corrected sinograms.
%%???%%
%%???%%

% This takes care of the plotting.
% It should 'just work' if provided with the correct kind of data.
h = figure();
subplot(1,2,1);
imshow(reconstruction,[]);
title('FBP');
subplot(1,2,2);
imshow(corrected_reconstruction,[]);
title('Corrected FBP');
waitfor(h);
