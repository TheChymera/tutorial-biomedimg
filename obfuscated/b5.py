import matplotlib.pylab as plt
import numpy as np
from os import path
from scipy.io import loadmat
from skimage.transform import iradon
from copy import deepcopy

# We dynamically determine the data path (the script may be run in place or via PythonTeX).
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")

# We load the sinogram.
sinogram = loadmat(path.join(data_dir,"phantom_sinogram.mat"))['arr']

# We select only the first 180 degree angular range
half = int(sinogram.shape[1]/2)
sinogram = sinogram[:,:half]

# We define a 0-180 degree angular range sampled corresponding to the sinogram.
theta = np.linspace(0., 180., max(sinogram.shape), endpoint=False)

# We define a figure for 10 detector reduction steps.
fig, ax = plt.subplots(2, 5)

# We iterate over 10 detector reduction steps
for j in range(10):

	# We do not start counting with 0.
	i = j+1

	# We determine the appropriate row and column to place the plot on.
	row = int(np.floor(j/5))
	column = j-row*5

	# We reconstruct the image with the appropriate reduction.
	##???##

	# This takes care of the plotting.
	# It should "just work" if provided with the correct kind of data.
	ax[row,column].imshow(reconstruction_fbp)
	ax[row,column].set_title('\#Detectors/'+str(i))
	ax[row,column].get_xaxis().set_visible(False)
	ax[row,column].get_yaxis().set_visible(False)
