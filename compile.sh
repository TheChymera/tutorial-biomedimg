#!/usr/bin/env bash

TARGET="${1}"
WHITELIST="
	handout.tex
	pres.tex
	"

if [ $TARGET = "all" ]; then
	for ITER_TARGET in *.tex; do
		if [[ $WHITELIST =~ (^|[[:space:]])$ITER_TARGET($|[[:space:]]) ]];then
			ITER_TARGET=${ITER_TARGET%".tex"}
			./compile.sh ${ITER_TARGET}
		fi
	done
else
	pdflatex -shell-escape ${TARGET}.tex &&\
	pythontex.py ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex &&\
	bibtex ${TARGET} &&\
	pdflatex -shell-escape ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex
fi
mkdir tutorial-biomedimg_exercises
cp handout.pdf tutorial-biomedimg_exercises/
cp -rf obfuscated tutorial-biomedimg_exercises/
cp -rf data tutorial-biomedimg_exercises/
zip -r tutorial-biomedimg_exercises.zip tutorial-biomedimg_exercises
rsync -avP tutorial-biomedimg_exercises.zip dreamhost:chymera.eu/docs/
